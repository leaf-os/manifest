## How to sync source code
### 1. Install repo
$ curl https://storage.googleapis.com/git-repo-downloads/repo > repo <br />
$ chmod a+rx ~/.bin/repo

### 2. Sync source
$ repo init -u https://gitlab.com/leaf-os/manifest.git <br />
$ repo sync
